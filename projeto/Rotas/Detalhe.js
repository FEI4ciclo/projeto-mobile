import React, {Component} from 'react';

import {
    View,StyleSheet,TouchableOpacity,Text,TextInput, Image,FlatList,SafeAreaView, AsyncStorage

} from "react-native";
import {ScrollView} from "react-navigation";
import awaitAsyncGenerator from "@babel/runtime/helpers/esm/awaitAsyncGenerator";

class Kid {
  constructor(name, age, gender, birthday, admission, parent, phone, ra){
    this.kidName ='nome ' + name;
    this.kidAge = 'age ' + age;
    this.kidGender = 'sexo ' + gender;
    this.kidBirthday = 'aniverssario ' + birthday;
    this.kidAdmission = 'admissão ' + admission;
    this.kidParent = 'nome do responsavel ' + parent;
    this.parentPhone = 'telefone do responsavel ' + phone;
    this.kidRa = 'RA ' + ra;

  }
}

class LogoTitle extends React.Component {
  render() {
    return (<Text
      style={{
        fontFamily:'Raleway-Black',
        fontSize: 40,
        color: 'white',
        textAlign: 'center',
        flex: 1,
      }}>KiDs App</Text>);
    }
  }

  const styles = StyleSheet.create({
      fundo: {
          flexWrap:"wrap"
      },
          list:{
              paddingVertical: 5,
              margin: 3,
              flexDirection: "row",
              alignSelf:"center",
              flexWrap: "wrap",
              backgroundColor: "white"
       }
      ,
      container :{
          backgroundColor: '#14b583',
          flex :1,

          justifyContent:'flex-start',
      },

      viewBotoes :{
          backgroundColor: '#14b583',
          justifyContent:'space-evenly',
          flexDirection:"row",

      },
      bIcon:{
          width:160,
          backgroundColor:'#149c71',
          borderRadius: 50,
          marginVertical:10,
          paddingVertical:10,
          paddingHorizontal:10,
          flexDirection:"row",

      },
      logoText :{
          fontSize:18,
          color : 'white'
      },
      inputBox:{
          fontFamily:'Raleway-Medium',
          width:300,
          backgroundColor:'white',
          borderRadius: 25,
          paddingHorizontal:16,
          marginVertical:10,
          alignSelf:"center"
      },

      buttonText:{
          color: 'white',
          paddingHorizontal:10,
          alignSelf:"center",
          fontFamily:'Raleway-Bold',
          fontSize:20,
          fontWeight:'500',
      },
      textStyle:{
          fontFamily:'Raleway-Medium',
          fontSize:25,
          fontWeight:'500',
          flexWrap: 'wrap'
      }
  })

class Detalhe extends Component {
    constructor(props){
        super(props);      
        this.mudouNome = this.mudouNome.bind(this);
        this.state = {nome:"", kids: [],busca:"", dados: []};
        this.cadastra=this.cadastra.bind(this);

        this.grava=this.grava.bind(this);
        this.leia=this.leia.bind(this);


    }

    grava(){
        AsyncStorage.setItem('meuApp_dados', [...this.state.kids]);
    }
    async leia(){
        let x = await AsyncStorage.getItem('meuApp_dados');
        this.setState({dados: x})
    }

    static navigationOptions = {
      headerStyle: {
        backgroundColor: '#14b583',
      },
      headerTitle: () => <LogoTitle></LogoTitle>
      ,
      headerRight: () => (<Image
        source={require('./kid.png')}
        style={{ width: 60, height: 60 }}
      />)
    };
    checaVazio(){
        if(this.state.nome == ''){
            alert('Vazio!')
        }else{
            this.edita(this.state.nome)
        }

    }
    mudouNome(txt){
      this.setState({nome: txt});   
    }

    cadastra(new_kid){
        let vetor = [...this.state.kids];
        const result = vetor.find( obj => obj.kidName === new_kid.kidName );
        if(result){
            alert('Kid Existe!')
        }else{
            this.setState({kids:  [...this.state.kids, new_kid ] });
            console.log(new_kid," adicionada ");
            this.props.navigation.pop();
        }
    }

    edita(kid){
      let vetor = [...this.state.kids];
        const result = vetor.find( obj => obj.kidName === kid );
        if(result){
            this.remove(kid)
            this.props.navigation.navigate("Ficha", {funcCadastra: this.cadastra})

        }else{
            alert('Kid nao existe!')
        }
        
    }

    remove(idx){
      let vetor = [...this.state.kids];
      console.log(this.state.kids," kids ");
      const result = vetor.find( obj => obj.kidName === idx );
      if(result){//se encontrou nome
        vetor.splice(result, 1);//remove
        console.log(result," removido ");
        this.setState({kids:  [...vetor] }  );
        console.log(this.state.kids, " kids ");
      }else{
        alert('Usuario nao encontrado!')
      }
    }


    busca(idx){

      let vetor = [...this.state.kids];
      
      const result = vetor.find( obj => obj.kidName === idx );
      if(result){
        while(vetor.length) {
          vetor.pop();
       }
        vetor.push(result);
        console.log(this.state.busca[0], "kid encontrado com sucesso");
        this.setState({busca:  [...vetor] }  );
        console.log(this.state.busca, "vetor busca");
      }else{
        alert('Usuario nao encontrado!')
      }
    }

    renderizaItem(item){
        return (
            <SafeAreaView>
                <View>
                <Text style={styles.textStyle}>Nome: {item.kidName}</Text>
                <Text style={styles.textStyle}>Idade: {item.kidAge}</Text>
                <Text style={styles.textStyle}>Sexo: {item.kidGender}</Text>
                <Text style={styles.textStyle}>Aniverssario: {item.kidBirthday}</Text>
                <Text style={styles.textStyle}>Admissao: {item.kidAdmission}</Text>
                <Text style={styles.textStyle}>Responsavel: {item.kidParent}</Text>
                <Text style={styles.textStyle}>Tel: {item.parentPhone}</Text>
                <Text style={styles.textStyle}>RA: {item.kidRa}</Text>
                </View>
                </SafeAreaView>
        )
    }

    render() {
    
          return (
            <View style={styles.container}>
                <View style={styles.viewBotoes}>   
                    <TouchableOpacity style={styles.bIcon} onPress={() => {this.props.navigation.navigate("Ficha", {funcCadastra: this.cadastra})}}>
                        <Image
                            style={{width: 35, height: 35, alignSelf:'center'}}
                            source={require('./add.png')}
                        />
                        
                        <Text style={styles.buttonText}>ADICIONA</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bIcon}  onPress={() => {this.remove(this.state.nome)}}>
                        <Image
                            style={{width: 35, height: 35, alignSelf:'center'}}
                            source={require('./del.png')}
                        />
                        <Text style={styles.buttonText}>DELETA</Text>
                    </TouchableOpacity>
                   
                </View>  

                <View style={styles.viewBotoes}> 
                    <TouchableOpacity style={styles.bIcon}  onPress={() => {this.busca(this.state.nome)}}>
                        <Image
                            style={{width: 35, height: 35, alignSelf:'center'}}
                            source={require('./srch.png')}
                        />
                        
                        <Text style={styles.buttonText}>BUSCA</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bIcon}  onPress={() => {this.checaVazio()}}>
                        <Image
                            style={{width: 35, height: 35, alignSelf:'center'}}
                            source={require('./edit.png')}
                        />
                        
                        <Text style={styles.buttonText}>EDITA</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.viewBotoes}>
                    <TouchableOpacity style={styles.bIcon}  onPress={() => {this.grava}}>
                        <Image
                            style={{width: 35, height: 35, alignSelf:'center'}}
                            source={require('./srch.png')}
                        />

                        <Text style={styles.buttonText}>Grava</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bIcon}  onPress={() => {this.leia}}>
                        <Image
                            style={{width: 35, height: 35, alignSelf:'center'}}
                            source={require('./srch.png')}
                        />

                        <Text style={styles.buttonText}>Leia</Text>
                    </TouchableOpacity>

                </View>
                
              

                
                <TextInput style={styles.inputBox} placeholder={"Digite um nome"} Type={"String"} id={'iNome'} onChangeText={this.mudouNome} />
                <View>
                  <FlatList
                    style={styles.list }
                    data={this.state.busca}
                    renderItem={ ({item}) =>  this.renderizaItem(item)}
                    keyExtractor={({kidName}, index) => kidName}
                  />

                </View>
                <SafeAreaView>

                    <FlatList
                        style={styles.list}
                        data={this.state.kids}
                        renderItem={ ({item}) =>  this.renderizaItem(item)}
                        keyExtractor={({kidName}, index) => kidName}
                    />
                </SafeAreaView>

            </View>
          );
    }
}

export default Detalhe;