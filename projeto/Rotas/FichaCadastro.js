import React, {Component} from 'react';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

import {
    View,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    Text,Image,
    AsyncStorage,
} from "react-native";

class LogoTitle extends React.Component {
    render() {
        return (<Text
                style={{
                    color: 'white',
                    fontSize: 40,
                    fontFamily:'Raleway-Black',
                    flex:1 ,
                    textAlign: 'center',
                    alignSelf: 'center'
                }}>Ficha</Text>

        );
    }
}

const styles = StyleSheet.create({
    container :{
        backgroundColor: '#14b583',
        justifyContent:'center',
        flex: 1,
        alignItems: 'center',

    },
    logoText :{
        fontSize:18,
        color : 'white'
    },
    inputBox:{
        fontFamily:'Raleway-Medium',
        width:300,
        backgroundColor:'white',
        borderRadius: 25,
        paddingHorizontal:16,
        marginVertical:10,
    },
    button:{
        width:300,
        backgroundColor:'#149c71',
        borderRadius: 25,
        marginVertical:10,
        paddingHorizontal:16,
        paddingVertical:16


    },buttonText:{
        fontFamily:'Raleway-Medium',
        fontSize:20,
        fontWeight:'500',
        color:'white',
        justifyContent:'center',
        textAlign:'center'

    },

})

var radio_props = [
    {label: 'Boy               ', value: 0 },
    {label: 'Girl', value: 1 }
];

class FichaCadastro extends Component {
    constructor(props){
        super(props);
        this.mudouNome = this.mudouNome.bind(this);
        this.mudouIdade = this.mudouIdade.bind(this);
        this.mudouSexo = this.mudouSexo.bind(this);
        this.mudouNiver = this.mudouNiver.bind(this);
        this.mudouAdm = this.mudouAdm.bind(this);
        this.mudouResp = this.mudouResp.bind(this);
        this.mudouTel = this.mudouTel.bind(this);
        this.mudouRa = this.mudouRa.bind(this);



        this.cadastra = this.cadastra.bind(this);
        //this.state={nome: "",idade:"", sexo: "M"};
        this.state={nome: "",idade:"", sexo: "M", niver: "", adm: "", resp: "", tel: "", ra: ""};
    }

    static navigationOptions = {
        headerStyle: {
            backgroundColor: '#14b583',
        },
        headerTitle: () => (<LogoTitle/>),
        headerRight: () => (<Image
            source={require('./kid.png')}
            style={{ width: 60, height: 60 }}
        />),
    };

    checaVazio(){
        if(this.state.nome == ''
           || this.state.idade == '' || this.state.sexo == '' || this.state.niver == '' || this.state.adm == ''
            || this.state.resp == '' || this.state.tel == '' || this.state.ra == ''
        ){
            alert('Vazio!')
        }else{
            this.cadastra();
        }
    }

    mudouNome(txt){
        this.setState({nome: txt});
    }

    mudouIdade(txt){
        this.setState({idade: txt});
    }
    mudouSexo(txt){
        if(txt = 0){
            this.setState({sexo: "M"})
        }else{
            this.setState({sexo: "F"})
        }
    }
    mudouNiver(txt){
        this.setState({niver: txt})
    }
    mudouAdm(txt){
        this.setState({adm: txt})
    }
    mudouResp(txt){
        this.setState({resp: txt})
    }
    mudouTel(txt){
        this.setState({tel: txt})
    }
    mudouRa(txt){
        this.setState({ra: txt})
    }
    cadastra(){
        this.props.navigation.getParam("funcCadastra")({kidName: this.state.nome, kidAge: this.state.idade, kidGender: this.state.sexo,
            kidBirthday: this.state.niver, kidAdmission: this.state.adm, kidParent: this.state.resp, parentPhone: this.state.tel,
            kidRa: this.state.ra});
    }
    render() {
        return (
            <View style={styles.container}>
                <TextInput style={styles.inputBox} placeholder={"Nome"} Type={"String"} id={'iNome'} onChangeText={this.mudouNome} />
                <TextInput style={styles.inputBox} placeholder={"Idade"} Type={"String"} keyboardType="numeric" id={'iIdade'} onChangeText={this.mudouIdade} />
                <RadioForm Style={styles}
                           radio_props={radio_props}
                           initial={0}
                           formHorizontal={true}
                           selectedButtonColor={"green"}
                           animation={true}
                           onPress={(value) => {this.mudouSexo({value:value})}}
                />
                <TextInput style={styles.inputBox} placeholder={"Aniverssario"} Type={"String"} keyboardType="numeric" id={'iNiver'} onChangeText={this.mudouNiver} />
                <TextInput style={styles.inputBox} placeholder={"Admissao"} Type={"String"} keyboardType="numeric" id={'iAdm'} onChangeText={this.mudouAdm} />
                <TextInput style={styles.inputBox} placeholder={"Responsavel"} Type={"String"} id={'iResp'} onChangeText={this.mudouResp} />
                <TextInput style={styles.inputBox} placeholder={"Telefone"} Type={"String"} keyboardType="numeric" id={'iTel'} onChangeText={this.mudouTel} />
                <TextInput style={styles.inputBox} placeholder={"RA"} Type={"String"} keyboardType="numeric" id={'iRa'} onChangeText={this.mudouRa} />


                <Text>{this.state.texto}</Text>
              

                <TouchableOpacity style={styles.button} title={'confirmar'} onPress={() => {this.checaVazio()}}>
                    <Text style={styles.buttonText}> Confirmar</Text>
                </TouchableOpacity>

            </View>
        );
    }
}
export default FichaCadastro;