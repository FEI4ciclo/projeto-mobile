import React, {Component} from 'react';
import {View, Text, Button, Image,StyleSheet,TouchableOpacity} from 'react-native';

class LogoTitle extends React.Component {
  render() {
    return (<Text
      style={{
        fontFamily:'Raleway-Black',
        color: 'white',
        fontSize: 40,
        textAlign: 'center',
        flex: 1,
      }}>KiDsApp</Text> 
      
    );
  }
}

const styles = StyleSheet.create({
  container :{
      backgroundColor: '#14b583',
      flex: 1,
      alignItems: 'center',
      

  },
  logoText :{
      fontSize:20,
      color : 'white'
  },
  inputBox:{
    fontFamily:'Raleway-Medium',
    width:300,
    backgroundColor:'white',
    borderRadius: 25,
    paddingHorizontal:16,
    marginVertical:10,
},
  button:{
      
      width:300,
      backgroundColor:'#149c71',
      borderRadius: 25,
      marginVertical:10,
      paddingHorizontal:16,
      paddingVertical:16
  },
  buttonText:{
      fontFamily:'Raleway-Medium',
      fontSize:20,
      fontWeight:'500',
      color:'white',
      justifyContent:'center',
      textAlign:'center'

  }
})

class Home extends Component {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: '#14b583',
    },
    headerTitle: () => <LogoTitle></LogoTitle>
    ,
    headerRight: () => (<Image
      source={require('./kid.png')}
      style={{ width: 60, height: 60 }}
    />),
    headerLeft: () => (
      <Button
        onPress={() => alert('App by Elias, Matheus, Lucas  . . . MonkeyLabs ™')}
        title="Info"
        color="#149c71"
      />
    ),
  };
  render() {
    
    return (
      <View style={styles.container}>
        {
          <Image
            style={{width: 250, height: 250}}
            source={require('./kid.png')}
          />
        }
      
        <TouchableOpacity style={styles.button} title={'Detalhes'} onPress={() => {this.props.navigation.navigate("Detalhes")}}>
                    <Text style={styles.buttonText}>Bem-vindo</Text>
        </TouchableOpacity>
        
        
      </View>);
  }
}

export default Home;